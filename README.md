Turtle syntax highlight for vim 
===============================

Copyright 
--------- 

Copyright (c) 2022 Luís Moreira de Sousa. All rights
reserved. Any use of this software constitutes full acceptance of all terms of
the document licence.

Description 
-----------

Syntax highlighting for the Turtle language in the vim file editor.

Installing 
----------

Start by cloning this repository:

```
git clone https://codeberg.org/ldesousa/vim-turtle-syntax.git
```

Then copy the syntax file to the `.vim` folder:

```
mkdir ~/vim/syntax
cp vim-turtle-syntax/turtle.vim ~/.vim/syntax/
```

Finally add the file type patterns to the `.vimrc` file:

``` 
au BufRead,BufNewFile *.ttl set filetype=turtle 
au BufRead,BufNewFile *.turtle set filetype=turtle 
```

Generating a new highlight file 
-------------------------------

The syntax highlight file included in this repository (`turtle.vim`) is
generated automatically with the programme `generate.py`. It parses the RDF
resources set in the `config` file and adds all the subjects found as vim
keywords to the highlighting. Currently, the `config` file points to OWL and
RDFS.

The `generate.py` programme relies on the RDFLib library. It can be run from a
virtual environment:

``` 
python3 -m venv env 
pip3 install -r requirements.txt 
python3 generate.py
```

Licence 
------- 

This suite of programmes is released under the EUPL 1.2
licence. For full details please consult the LICENCE file.
