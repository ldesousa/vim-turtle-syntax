# Generates a vim syntax highlight file for the Turtle language. It parses the
# RDF resources set in the config file and adds all the subjects found as
# keyworks to the syntax.
#
# This programme relies on the RDFLib library. It can be run from a virtual
# environment:
#
# python3 -m venv env
# pip3 install -r requirements.txt
# python3 generate.py
#
# Copyright (c) 2022 Luís Moreira de Sousa
#
# SPDX-License-Identifier: EUPL-1.2


import configparser
from rdflib import Graph

def makeKeywords(uri, key_type):

    keywords = ""
    g = Graph()
    g.parse(uri)
    #print(len(owl))

    query = """
    SELECT DISTINCT ?subject
    WHERE {?subject ?p ?o}
    """

    qres = g.query(query)
    for row in qres:
        # This try block eliminates empty subjects    
        try:
            sub = row.subject.split('#')[1]
        except:
            continue
        if len(sub) > 0:
            keywords += f"\nsyn keyword {key_type} {sub}"

    return keywords


if __name__ == "__main__":

    config = configparser.ConfigParser()
    config.read('config')
    options = config.options('URI')

    keywords = ""
    for option in options:
        print(f"Fetching {option} predicates ...")
        keywords += f"\n\n\" {option} predicates"
        keywords += makeKeywords(config.get('URI', option), option)

    # Read highlight init
    init = ""
    with open("turtle.vim.init","r") as file:
        init = file.readlines()

    # Create new highlight file
    with open("turtle.vim","w") as file:
        for line in init:
            file.write(line)
        file.write(keywords)

    print(f"All done!")
